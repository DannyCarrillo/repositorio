<?php
  require_once 'php/conexion.php';
  session_start();
 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <link rel="stylesheet" href="css/estilo.css">
     <link rel="stylesheet" href="./css/bootstrap.min.css" >
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


     <title></title>
   </head>
   <body>
     <nav class="navbar navbar-light " id="navbar-nav">
       <a class="navbar-brand" href="menu.php" >
         <img src="./img/Pikachu.png" width="30" height="30" class="d-inline-block align-top" alt="">
        POKEMON
       </a>
       </nav>

       <div class="container-fluid">
         <div class="row" >
           <div class="col-12" style="padding:30px" >
              <h1 class="display-3">BATTALLA POKEMON</h1>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="form-group">
               <label for="pokemon">Escoge tu pokemon</label>
                 <select id="pokemon" name="pokemon"  class="form-control text-center">
                    <option selected>Selecciona tu pokemon</option>
                    <?php
                    $busquedaEntre = $mysqli->query("select * from equipo where idEntrenador = '".$_SESSION['user']."'");
                    while ($row = $busquedaEntre->fetch_assoc() ) {
                      $busquedaP = $mysqli->query("select * from pokemon where nPokedex = '".$row['nPokedex']."'");
                      while ($filap = $busquedaP->fetch_assoc()) {
                        ?><option > <img src="<?php echo $filap['imagenP'];?>" alt=""> <?php echo $filap['nPokedex']." - ".$filap['nombreP'];?></option><?php
                      }
                      $busquedaP->close();
                    } ?>
                   </select>
                   <label for="nivel">Escoge el nivel de la batalla</label>
                   <select id="nivel" name="nivel" class="form-control text-center">
                      <option selected>Bajo</option>
                      <option>Medio</option>
                      <option>Dificil</option>
                   </select>

                   <div  style="padding:8px">
                   <button type="submit" id="bnEnt" onclick="buscarOpone();" class="btn btn-warning">Batallar</button>
                   </div>

                </div>
            </div>
          </div>
         <div class="row">
           <div class="col-lg-12">
             <div class="row">
               <div class="col-lg-12">
                 <h4 class=" text-center mb-1"> Equipo pokemon</h4>
               </div>
             </div>
           </div>
             <div class="row">
               <div class="col-lg-12">
                 <div class="card-group">
                   <?php
                   $busquedaEntre = $mysqli->query("select * from equipo where idEntrenador = '".$_SESSION['user']."'");
                   while ($row = $busquedaEntre->fetch_assoc() ) {
                     $busquedaP = $mysqli->query("select * from pokemon where nPokedex = '".$row['nPokedex']."'");
                     while ($filap = $busquedaP->fetch_assoc()) { ?>
                        <div class="card" style="width: 20rem;">
                          <img class="card-img-top" src="<?php echo  $filap['imagenP']; ?>" alt="Card image cap">
                             <div class="card-body">
                               <h4 class="card-title"><?php echo $filap['nombreP']; ?></h4>
                             </div>
                           </div><?php
                         }
                       } ?>
                    </div>
                  </div>
                </div>
               </div>
             </div>

       <script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" ></script>
       <script src="./js/bootstrap.min.js" ></script>
       <script type="text/javascript">
         function buscarOpone() {
           var nivel = document.getElementById('nivel')[0].value;

           var para = {
             'niv': nivel
           };

           $.ajax({
              url: "php/buscarOponente.php",
              type: 'POST',
              data: para,
              success: function(responser){
                console.log(responser);
              }
           });
         }
       </script>
   </body>
 </html>
